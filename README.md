# Kerberos-enabled SSH for automated login to IGWN clusters

This Docker image provides ssh login to IGWN clusters to enable continuous
deployment in [GitLab CI](https://git.ligo.org/help/ci/quick_start/index.md).

To use it, you will need to first need a Kerberos keytab. Assuming that it is
saved to the file `keytab.conf`, first convert it to base64 format using the
following command:

    $ base64 keytab

Next, store the base64-encoded keytab in a
[CI/CD variable](https://git.ligo.org/help/ci/variables/index.md). When you
create the CI/CD variable:

* Enter `SSH_KEYTAB_BASE64` for the key.
* Paste the output of the `base64 keytab` command into the value.
* Select `Variable` from the `Type` menu.
* Check `Protect variable`.

Finally you can add a CI job to your project's `.gitlab-ci.yml` file such as
the following:

```yaml
deploy:
  image: containers.ligo.org/emfollow/ssh-kerberos
  script:
    - |
      ssh emfollow-playground@emfollow-playground.ligo.caltech.edu bash <<EOF
      echo 'These commands are running on the cluster'
      ls
      # etc.
      EOF
        
```

Or, if you specify the `SSH_DESTINATION` variable, then you don't even have to
call `ssh` explicitly:

```yaml
deploy:
  image: containers.ligo.org/emfollow/ssh-kerberos
  variables:
    SSH_DESTINATION: emfollow-playground@emfollow-playground.ligo.caltech.edu
  script:
    - echo 'These commands are running on the cluster'
    - ls
    # etc.
```

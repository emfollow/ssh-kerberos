FROM debian:stable-slim

RUN apt-get update && \
    apt-get -y install --no-install-recommends \
        krb5-user \
        ssh-client \
        && \
    rm -rf /var/lib/apt/lists/* && \
    echo '@cert-authority * ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHa03AZF3CvJ1C4Po15swSaMYI4kPszyBH/uOKHQYvu+EpehSfMZMaX5D7pUpc5cAXvMEEFzlZJQH4pOioIlqyE= IGWN_CIT_SSH_CERT_AUTHORITY' \
        >> /etc/ssh/ssh_known_hosts

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

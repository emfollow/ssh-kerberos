#!/bin/sh
set -e

if [ -n "${SSH_KEYTAB_BASE64}" ]
then
    echo "${SSH_KEYTAB_BASE64}" | base64 -d | install -m u+r /dev/stdin /etc/krb5.keytab
    klist -k
    PRINCIPAL=$(klist -k | head -n 4 | tail -n 1 | sed -E 's/^.* +//')
    kinit -k "${PRINCIPAL}"
else
    echo 'No keytab found'
fi

if [ -n "${SSH_DESTINATION}" ]
then
    exec ssh "${SSH_DESTINATION}" "${@}"
else
    exec "${@:-bash}"
fi
